![logo](./docs/images/logo.png)

# GITMATE 2 DOCUMENTATION

![build status](https://gitlab.com/gitmate/open-source/gitmate-2/badges/master/build.svg)

User facing documentation of GitMate 2.


Visit https://docs.gitmate.io to know more about Gitmate 2.

## Requirements

- [mkdocs](http://www.mkdocs.org/)
- [mkdocs-material](http://squidfunk.github.io/mkdocs-material/)

Run `pip install -r requirements.txt` to install these requirements.

## Testing

To server the docs run `mkdocs serve`. This command will serve the docs on https://localhost:8000 .
The server will automatically pick up the changes.

Run `mkdocs build` to build the docs. This will generate the static site in `site` directory.
