# Finding Duplicate Issues

GitMate is trained to find issues that look similar and notify the user immediately.

To use this feature activate `Show possible duplicates` setting of `Find Similar Issues` plugin.

![image](../images/duplicate_issues_plugin.png)

If a newly opened issue looks similar to any of the existing ones, GitMate will post a link to the
 likely duplicates in a comment of the new issue. This helps maintainers to find and remove duplicate issues.
Users can instantaneously find helpful information in the discussions of the linked similar issues.
The maintainers can easily find bug reports and feature requests which are similar, and use the discussions on various aspects of requests or simply remove the duplicate issues.

![image](../images/duplicate_issues.png)
