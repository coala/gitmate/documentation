# Getting Started with GitMate 2

The purpose of this doc is to guide you to set up GitMate on your projects
on [supported platforms](../#supported_platforms).

### 1. Login

Login on [GitMate 2](https://gitmate.io) with your favourite platform.

### 2. Activate GitMate 2 on a repo

Activate Gitmate 2 on a repo by simply clicking on the slider button next to your
repository name. This will register a webhook on that repository.

![image](../images/gitmate-repo-view.png)

### 3. Activate the plugins

Next step is to activate the plugins.
Click on settings icon of your repository to see the list of available plugins.
Activate individual plugins and provide the appropriate settings.

GitMate 2 is now configured on your repository.

!!! note
    See "Features" for a list of features.
